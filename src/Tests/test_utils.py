from cmath import sqrt
from src import utils

def test_is_prime():
    assert utils.isPrime(4) == False
    assert utils.isPrime(2) == True
    assert utils.isPrime(3) == True
    assert utils.isPrime(8) == False
    assert utils.isPrime(10) == False
    assert utils.isPrime(7) == True
    assert utils.isPrime(0) == False
    assert utils.isPrime(1) == False
    assert utils.isPrime(2.3) == True
    assert utils.isPrime(-3), "Negative numbers are not allow"

def test_cubic():
    assert utils.cubic(2) == 8
    assert utils.cubic(-2) == -8
    assert utils.cubic(-4) != 8
    assert utils.cubic(4) != -8

def test_say_hello():
    assert utils.say_hello("Lulu") == "Hello Lulu"
    assert utils.say_hello("Opopop") == "Hello Opopop"
    assert utils.say_hello("AAAAA") == "Hello AAAAA"
    assert utils.say_hello(22), "numbers are not allowed"
    assert utils.say_hello("bbbbbbbb") != "aaaaaaa"